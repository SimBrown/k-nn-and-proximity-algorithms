import csv
import math
from random import shuffle

#computes the euclidean distance
def euclideanDistance(sample, sample_test):
    distance = 0
    for x in range(len(sample)-1):#last value is excluded in distance computation
        distance += pow((float(sample[x]) - float(sample_test[x])), 2)
    return math.sqrt(distance)    

#computes the absolute distance 
def absoluteDifferenceDistance(sample, sample_test):
    distance = 0
    for x in range(len(sample)-1):#last value is excluded in distance computation
        distance += abs(float(sample[x]) - float(sample_test[x]))
    return distance  

#return k neighbors to the sample test
def getK_Neighbors(sample_test,k):
    #contains discovered neighbors
    neighbors=[]
    
    #parsing TS looking for close matches
    for x in range(len(TS)):
        distance = absoluteDifferenceDistance(TS[x],sample_test)
        neighbors.append([int(distance),TS[x][-1]])
        #sorting list to extract k closest elements
        neighbors.sort()
    #returning only k elements
    return neighbors[:k]
    
#test Accuracy of prediction against TestSet
def getAccuracy(predictions):
    correct = 0
    for x in range(len(TestSet)-1):
        #[-1] means that tuple will be indexed from the end
        if str(TestSet[x][-1]) == str(predictions[x]):
            correct += 1
    return str((correct/float(len(TestSet))) * 100.0)

#get average value from neighbors
def getAverage(neighbors):
    m=0
    b=0
    for n in neighbors:
        if(str(n[-1])=='2'): b+=1
        else: m+=1
        
    return 4 if m>b else 2

#run the algorithm
def runKnn(k):
    predictions=[]
    neighbors=()
    
    for sample in TestSet:
        neighbors = getK_Neighbors(sample,k)
        predictions.append(getAverage(neighbors))

    return getAccuracy(predictions)

#runs cycles of tests
def k_fold_test(k,cycles,perc):
    
    global file
    global TS
    global TestSet
    global Split
    Split=perc
    val=0
    
    for x in range(cycles):
        TS=[]
        TestSet=[]
        #sorting by random
        shuffle(file)
    
        #taking TS lines 80% of total
        ts_lines = int((len(file)-1)*Split)
        
        for line in range(ts_lines):
            TS.append(file[line])
            
        #populating Test Set
        for line in range(len(file)-ts_lines):
            TestSet.append(file[line])
            
        TestSet.sort()
        current=float(runKnn(k))
        #print(str(current)+"%")
        val+=float(current)
            
    print("Average: "+str(val/cycles)+"%")

    
with open('../../Breast-dataset/breast-cancer-wisconsin.data', 'r') as csv_file:
    
    file=[]
    TS=[]
    TestSet=[]
    Split=0.80 # percentage of TS
    i=0
    
    csv_reader = csv.reader(csv_file, delimiter=',')
    
    #dataset includes some '?' values for unknown values
    #we remove those rows in order to prevent exception in data manipulation
    for line in csv_reader:
        #excluding first column which contains IDs
        #factor each line into tuple for immutability
        #data manipulation on tuple is much faster than on lists
        if not '?' in line:
            file.insert(i,tuple(line[1:])) 
            i+=1
    
    #sorting by random
    shuffle(file)

    #taking TS lines 80% of total
    ts_lines = int((len(file)-1)*Split)
    
    for line in range(ts_lines):
        TS.append(file[line])
        
    #populating Test Set
    for line in range(len(file)-ts_lines):
        TestSet.append(file[line])
        
    TestSet.sort()
    
    #range of feature values based on first feature
    RANGE = set(range(len(TS[0])))
    
    
