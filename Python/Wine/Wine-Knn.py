import csv
import math
from random import shuffle
    
#computes the euclidean distance          
def euclideanDistance(sample, sample_test):
    distance = 0
    for x in range(1,len(sample)-1):#first value is excluded in distance computation
        distance += pow((float(sample[x]) - float(sample_test[x])), 2)
    return math.sqrt(distance)
   
#computes the absolute distance 
def absoluteDifferenceDistance(sample, sample_test):
    distance = 0
    for x in range(1,len(sample)):#first value is excluded in distance computation
        distance += abs(float(sample[x]) - float(sample_test[x]))
    return distance

#return k neighbors to the sample test
def getK_Neighbors(sample_test,k):
    #contains discovered neighbors
    neighbors=[]
    
    #parsing TS looking for close matches
    for x in range(len(TS)):
        distance = euclideanDistance(TS[x],sample_test)
        neighbors.append([int(distance),TS[x][0]])
        #sorting list to extract k closest elements
        neighbors.sort()
    #returning only k elements
    return neighbors[:k]
    

#test Accuracy of prediction against TestSet
def getAccuracy(predictions):
    correct = 0
    for x in range(len(TestSet)-1):
        #[-1] means that tuple will be indexed from the end
        if str(TestSet[x][0]) == str(predictions[x]):
            correct += 1
    return str((correct/float(len(TestSet))) * 100.0)

#get average value from neighbors
def getAverage(neighbors):
    a=0
    b=0
    c=0
    for n in neighbors:
        if(str(n[-1])=='1'): a+=1
        if(str(n[-1])=='2'): b+=1
        if(str(n[-1])=='3'): c+=1
    
    max_val = max(a,b,c)
    
    if max_val == a : return 1
    if max_val == b : return 2
    if max_val == c : return 3        
 
#run the algorithm
def runKnn(k):
    predictions=[]
    neighbors=()
    
    for sample in TestSet:
        neighbors = getK_Neighbors(sample,k)
        predictions.append(getAverage(neighbors))
    
    return getAccuracy(predictions)

#runs cycles of tests
def k_fold_test(k,cycles,perc):
    
    global file
    global TS
    global TestSet
    global Split
    Split=perc
    val=0
    
    for x in range(cycles):
        TS=[]
        TestSet=[]
        #sorting by random
        shuffle(file)
    
        #taking TS lines 80% of total
        ts_lines = int((len(file)-1)*Split)
        
        for line in range(ts_lines):
            TS.append(file[line])
            
        #populating Test Set
        for line in range(len(file)-ts_lines):
            TestSet.append(file[line])
            
        TestSet.sort()
        current=float(runKnn(k))
        #print(str(current)+"%")
        val+=float(current)
            
    print("Average: "+str(val/cycles)+"%")


    
with open('../../Wine-dataset/wine.data', 'r') as csv_file:
    
    file=[]
    TS=[]
    TestSet=[]
    Split=0.80 # percentage of TS
    i=0
    
    csv_reader = csv.reader(csv_file, delimiter=',')
    
    #dataset includes some '?' values for unknown values
    #we remove those rows in order to prevent exception in data manipulation
    for line in csv_reader:
        #excluding first column which contains IDs
        #factor each line into tuple for immutability
        #data manipulation on tuple is much faster than on lists
        if not '?' in line:
            file.insert(i,tuple(line[0:])) 
            i+=1
    
    #sorting by random
    shuffle(file)

    #taking TS lines 80% of total
    ts_lines = int((len(file)-1)*Split)
    
    for line in range(ts_lines):
        TS.append(file[line])
        
    #populating Test Set
    for line in range(len(file)-ts_lines):
        TestSet.append(file[line])
        
    TestSet.sort()
    
    #range of feature values based on first feature
    RANGE = set(range(len(TS[0])))